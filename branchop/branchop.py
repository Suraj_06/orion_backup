from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator
from datetime import datetime

# Define the function to determine the branch
def decide_branch(**kwargs):
    current_date = kwargs['execution_date']
    if current_date.minute % 2 == 0:
        return 'even_branch'
    else:
        return 'odd_branch'

# Define the DAG
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2024, 4, 24),
}

dag = DAG(
    'branchop',
    default_args=default_args,
    description='A simple DAG demonstrating branching',
    schedule_interval='*/1 * * * *',
)

# Define tasks
start_task = DummyOperator(task_id='start_task', dag=dag)
end_task = DummyOperator(task_id='end_task', dag=dag)

branch_task = BranchPythonOperator(
    task_id='branch_task',
    python_callable=decide_branch,
    provide_context=True,
    dag=dag,
)

even_branch_task = DummyOperator(task_id='even_branch_task', dag=dag)
odd_branch_task = DummyOperator(task_id='odd_branch_task', dag=dag)

# Define the task dependencies
start_task >> branch_task
branch_task >> [even_branch_task, odd_branch_task]
[even_branch_task, odd_branch_task] >> end_task

