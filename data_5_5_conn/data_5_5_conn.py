from datetime import datetime, timedelta
import pendulum
# The dags object; we'll need this to instantiate a dags
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
# Operators; we need this to operate!
from airflow.operators.dummy_operator import DummyOperator

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
from op_project.datamart import build_data_catalog
# from op_core.dag_generator_master import operations




# processing_time = '{{execution_date.timestamp()}}'

default_args = {
    'owner': 'NUBAX',
    'depends_on_past': False,
    'email': ['yogesh.prabhu@nubaxdatalabs.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 5,
    'retry_delay': timedelta(minutes=5),
    # 'start_date': datetime.now(),

    # 'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}
with DAG(
        "data_5_5_conn",
        default_args=default_args,
        description="this is DAG for data_5_5_conn",
        schedule_interval=None,
        #schedule_interval=datetime.timedelta(
        # schedule_interval=None,
        start_date=pendulum.datetime(2024, 4, 26, 17, tz='Asia/Calcutta'),
        # start_date=datetime(2021, 1, 1),
        catchup=False,
        tags=None,
) as dag:


    #dag.is_paused_upon_creation=False
    start = DummyOperator(
        task_id='start',
        dag=dag
    )
    end = DummyOperator(
        task_id='end',
        dag=dag
    )


# handled dynamic tasks for different featues
    # for function_name in operations:
    #     func = globals().get(function_name)
    #     if func:
    #         task_id = function_name
    #         task = PythonOperator(
    #             task_id=task_id,
    #             python_callable=func
    #         )
    #         start >> task
    #
    # task >> end


    loading_full = PythonOperator(
        task_id="build_data_catalog",
        python_callable=build_data_catalog,
        dag = dag,
        op_kwargs ={'conn_id': 354}
    )

    start >> loading_full >> end





