from datetime import datetime, timedelta
import pendulum
# The dags object; we'll need this to instantiate a dags
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
# Operators; we need this to operate!
from airflow.operators.dummy_operator import DummyOperator
import importlib
from op_project.datamart import delete_s3_object

m_operations = ['build_master_null', 'build_transformation', 'build_master_typecast']
for module_name in m_operations:
    module = importlib.import_module(f"op_project.datamart")
    globals()[module_name] = getattr(module, module_name)

m_ids = ['8', '17', '9']


# processing_time = '{{execution_date.timestamp()}}'

default_args = {
    'owner': 'NUBAX',
    'depends_on_past': False,
    'email': ['yogesh.prabhu@nubaxdatalabs.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 5,
    'retry_delay': timedelta(minutes=5),
    # 'start_date': datetime.now(),

    # 'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}
with DAG(
        "op1",
        default_args=default_args,
        description="this is DAG for op1",
        schedule_interval="15 * * * *",
        #schedule_interval=datetime.timedelta(
        # schedule_interval=None,
        start_date=pendulum.datetime(2024, 4, 12, 15, tz='Asia/Calcutta'),
        # start_date=datetime(2021, 1, 1),
        catchup=False,
        tags=None,
) as dag:


    #dag.is_paused_upon_creation=False
    start = DummyOperator(
        task_id='start',
        dag=dag
    )
    end = PythonOperator(
        task_id='deleting_s3_object',
        python_callable = delete_s3_object,
        dag=dag,
        op_kwargs = {'p_name': 'op1'}
    )

    m_operations = ['build_master_null', 'build_transformation', 'build_master_typecast']
    #checking if destination id redshift i.e if fifth parameter is 1 passed from node.js bash script will execute
    op = 0
    if op == 1:
#        dict = $params
#        p_id = dict["pipe_id"]
        p_id = int(m_ids[0])
        bash_command = f'bash /home/yogesh_prabhu/etc/op_core/bash_redshift.sh "{p_id}" '
        #bash_command = f'bash /home/yogesh_prabhu/bash_redshift.sh '
        loading_full = BashOperator(
            task_id='execute_bash_script',
            bash_command=bash_command,
            dag=dag,
        )

    else:
        previous_task = start
        params_list = [{'pipe_id': int(num)} for num in m_ids]  
        # Loop through function names and create tasks
        for function_name, params in zip(m_operations, params_list):
            func = globals().get(function_name)
            if func:
                task_id = function_name
                task = PythonOperator(
                    task_id = task_id,
                    python_callable = func,
                    dag = dag,
                    op_kwargs = params
            )
                # Set dependencies between tasks
                if previous_task:
                    previous_task >> task  # This sets the current task to run after the previous task
                previous_task = task






        # Set end task as a dependent task of build_master
        if previous_task == start:
            start >> end
        else:
            previous_task >> end

