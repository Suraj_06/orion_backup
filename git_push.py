from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime
import os
import subprocess
subprocess.run(['pip', 'install', 'gitpython'])
import git


# Function to upload folder to Git repository
def upload_folder_to_git():
    # Path to the folder to upload
    folder_path = "/home/yogesh_prabhu/airflow/dags/op_project/utils"
    
    # URL of the Git repository
    repo_url = "https://gitlab.com/Suraj_06/test_sink.git"

    # Clone the repository to a temporary directory
    temp_dir = "/tmp/git_temp"
    repo = git.Repo.clone_from(repo_url, temp_dir)

    # Copy the folder content into the cloned repository
    folder_content = os.listdir(folder_path)
    for item in folder_content:
        item_path = os.path.join(folder_path, item)
        if os.path.isfile(item_path):
            os.system(f"cp {item_path} {temp_dir}")
        elif os.path.isdir(item_path):
            os.system(f"cp -r {item_path} {temp_dir}")

    # Add all files in the folder to the staging area
    repo.git.add(all=True)

    # Commit changes
    repo.git.commit('-m', 'Uploaded folder via Airflow')

    # Push changes to remote repository
    origin = repo.remote(name='origin')
    origin.push()

    # Clean up the temporary directory
    #os.system(f"rm -rf {temp_dir}")

# Define your Airflow DAG
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2024, 5, 1),
    'retries': 1,
}

dag = DAG(
    'upload_folder_to_git',
    default_args=default_args,
    description='Upload a folder to Git repository',
    schedule_interval=None,
)

# Define the task
upload_task = PythonOperator(
    task_id='upload_folder_task',
    python_callable=upload_folder_to_git,
    dag=dag,
)

# Set task dependencies if necessary
# upload_task.set_upstream(...)

