#!/bin/bash

# Define dependencies between tasks using Airflow CLI commands
airflow tasks set-downstream atry  op1_task op2_task op3_task
airflow tasks set-downstream atry op2_task op4_task
airflow tasks set-downstream atry op3_task op5_task
airflow tasks set-downstream atry op4_task op6_task
airflow tasks set-downstream atry op5_task op6_task

