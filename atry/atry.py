from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.models.baseoperator import chain


# Define functions for each operation
def op1():
    print("Executing op1")

def op2():
    print("Executing op2")

def op3():
    print("Executing op3")

def op4():
    print("Executing op4")

def op5():
    print("Executing op5")

def op6():
    print("Executing op6")

# Define the default arguments for the DAG
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2024, 4, 18),
    'retries': 1,
}

# Define the DAG
dag = DAG(
    'atry',
    default_args=default_args,
    description='A simple DAG with custom dependencies',
    schedule_interval=None,
)

# Define tasks for each operation
op1_task = PythonOperator(
    task_id='op1_task',
    python_callable=op1,
    dag=dag,
)

op2_task = PythonOperator(
    task_id='op2_task',
    python_callable=op2,
    dag=dag,
)

op3_task = PythonOperator(
    task_id='op3_task',
    python_callable=op3,
    dag=dag,
)

op4_task = PythonOperator(
    task_id='op4_task',
    python_callable=op4,
    dag=dag,
)

op5_task = PythonOperator(
    task_id='op5_task',
    python_callable=op5,
    dag=dag,
)

op6_task = PythonOperator(
    task_id='op6_task',
    python_callable=op6,
    dag=dag,
)

# Set up task dependencies
#op1_task >> [op2_task, op3_task]
#op2_task >> op4_task
#op3_task >> op5_task
#[op4_task, op5_task] >> op6_task



