from datetime import datetime, timedelta
import pendulum
# The dags object; we'll need this to instantiate a dags
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
# Operators; we need this to operate!
from airflow.operators.dummy_operator import DummyOperator
import importlib
from op_project.datamart import delete_s3_object

m_operations = ['transformation_1', 'null_1', 'duplicates_1', 'duplicates_2', 'null_2', 'typecast_1']
for module_name in m_operations:
    if module_name.startswith('transformation'):
        module_name = 'build_' + module_name.split('_')[0]
    else:
        module_name = 'build_master_' + module_name.split('_')[0]
    module = importlib.import_module(f"op_project.datamart")
    globals()[module_name] = getattr(module, module_name)

m_ids = ['1', '1', '1', '1', '1', '1']


# processing_time = '{{execution_date.timestamp()}}'

default_args = {
    'owner': 'NUBAX',
    'depends_on_past': False,
    'email': ['yogesh.prabhu@nubaxdatalabs.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 5,
    'retry_delay': timedelta(minutes=5),
    # 'start_date': datetime.now(),

    # 'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
    # 'trigger_rule': 'all_success'
}
with DAG(
        "ap_name",
        default_args=default_args,
        description="this is DAG for ap_name",
        schedule_interval="15 * * * *",
        #schedule_interval=datetime.timedelta(
        # schedule_interval=None,
        start_date=pendulum.datetime(2024, 4, 25, 16, tz='Asia/Calcutta'),
        # start_date=datetime(2021, 1, 1),
        catchup=False,
        tags=None,
) as dag:


    #dag.is_paused_upon_creation=False
    start = DummyOperator(
        task_id='start',
        dag=dag
    )

    end = DummyOperator(
        task_id= 'end',
        dag= dag
    )
    #end = PythonOperator(
    #    task_id='deleting_s3_object',
    #    python_callable = delete_s3_object,
    #    dag=dag,
    #    op_kwargs = {'p_name': 'ap_name'}
    #)
    #tasks = start >> tasks['transformation_1'] >> [tasks['null_1'], tasks['duplicates_1']]
#tasks['null_1'] >> tasks['duplicates_2']
#tasks['duplicates_1'] >> tasks['null_2']
#[tasks['duplicates_2'], tasks['null_2']] >> tasks['typecast_1'] >> end
    m_operations = ['transformation_1', 'null_1', 'duplicates_1', 'duplicates_2', 'null_2', 'typecast_1']
    #checking if destination id redshift i.e if fifth parameter is 1 passed from node.js bash script will execute
    op = 0
    if op == 1:
#        dict = $params
#        p_id = dict["pipe_id"]
        p_id = int(m_ids[0])
        bash_command = f'bash /home/yogesh_prabhu/etc/op_core/bash_redshift.sh "{p_id}" '
        #bash_command = f'bash /home/yogesh_prabhu/bash_redshift.sh '
        loading_full = BashOperator(
            task_id='execute_bash_script',
            bash_command=bash_command,
            dag=dag,
        )

    else:
         tasks = {}
         params_list = [{'pipe_id': int(num)} for num in m_ids]

         for function_name, params in zip(m_operations, params_list):
           if function_name:
             task_id = function_name
             function_to_execute = globals().get(function_name)

             # Handle specific case for transformation functions
             if 'transformation' in function_name:
                function_to_execute = build_transformation
             elif 'null' in function_name:
                function_to_execute = build_master_null
             elif 'duplicates' in function_name:
                function_to_execute = build_master_duplicates
             elif 'typecast' in function_name:
                function_to_execute = build_master_typecast

             if function_to_execute:
               task = PythonOperator(
                 task_id=task_id,
                 python_callable=function_to_execute,
                 dag=dag,
                 op_kwargs=params
               )
               tasks[task_id] = task  # Store tasks in a dictionary
               print(f"Created task with ID: {task_id}")

         start >> tasks['transformation_1'] >> [tasks['null_1'], tasks['duplicates_1']]
tasks['null_1'] >> tasks['duplicates_2']
tasks['duplicates_1'] >> tasks['null_2']
[tasks['duplicates_2'], tasks['null_2']] >> tasks['typecast_1'] >> end









